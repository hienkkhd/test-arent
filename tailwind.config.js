/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    colors: {
      light: "#FFFFFF",
      primary300: "#FFCC21",
      primary400: "#FF963C",
      primary500: "#EA6C00",
      secondary300: "#8FE9D0",
      gray400: "#777777",
      dark400: "#707070",
      dark500: "#414141",
      dark600: "#2E2E2E",
    },
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "0px",
        xl: "0px",
        "2xl": "0px",
      },
      screens: {
        lg: "960px",
        xl: "960px",
        "2xl": "960px",
      },
    },
    extend: {},
  },
  plugins: [],
};
