# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
   parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
   },
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
### Run Project Guide
Project use vite and json-server
Create .env (follow .env.example) to change API url (default: http://localhost:3004)
```
   ### Step 1: Install package.json
   npm i
   ### Step 2: Run json-server
   npm run server
   ### Step 3: Run project
   npm run dev
```

### Progress Project

- [X] Create Base: 2023/07/25 23:00 =>  2023/07/25 23:30 
- [X] Home Page (UI): 2023/07/25 23:30 =>  2023/07/26 02:00 
- [X] MyRecord (UI): 2023/07/26 21:00 =>  2023/07/26 22:15 
- [X] Columns (UI): 2023/07/26 22:15 =>  2023/07/26 23:00 
- [X] Add db.json (SERVER): 2023/07/27 19:45 =>  2023/07/27 20:00 
- [X] Add Api for each Page (Api): 2023/07/27 20:00 =>  2023/07/27 22:00
- [X] Add back to top button (UI): 2023/07/27 22:00 =>  2023/07/27 22:05 
- [X] Write READEME (UI): 2023/07/27 22:05 =>  2023/07/26 22:15 
