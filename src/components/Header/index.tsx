import logo from "../../assets/Imagine/logo.png";
import iconMemo from "../../assets/icons/icon_memo.png";
import iconChallenge from "../../assets/icons/icon_challenge.png";
import iconInfo from "../../assets/icons/icon_info.png";
import iconMenu from "../../assets/icons/icon_menu.png";
import { routes } from "../../routes/root";
import { useNavigate } from "react-router-dom";
import Drawer from "react-modern-drawer";
import { useState } from "react";

function Header() {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const toggleDrawer = () => {
    setIsOpen((prevState) => !prevState);
  };
  const menuDrawer: { label: string; path: string }[] = [
    {
      label: "自分の記録",
      path: routes.MY_RECORD,
    },
    {
      label: "体重グラフ",
      path: "",
    },
    {
      label: "目標",
      path: "",
    },
    {
      label: "選択中のコース",
      path: "",
    },
    {
      label: "コラム一覧",
      path: routes.COLUMNS,
    },
    {
      label: "設定",
      path: "",
    },
  ];
  return (
    <header className="bg-dark500 w-full ">
      <div className="container flex justify-between items-center">
        <div className="cursor-pointer" onClick={() => navigate(routes.HOME)}>
          <img width={144} height={64} src={logo} />
        </div>
        <nav className="flex gap-10">
          <div
            className=" gap-2 items-center cursor-pointer hidden md:flex"
            onClick={() => navigate(routes.MY_RECORD)}
          >
            <img width={32} height={32} src={iconMemo} />
            <p className="text-light">自分の記録</p>
          </div>
          <div className=" gap-2 items-center cursor-pointer hidden md:flex">
            <img width={32} height={32} src={iconChallenge} />
            <p className="text-light">自分の記録</p>
          </div>
          <div className=" gap-2 items-center cursor-pointer hidden md:flex">
            <img width={32} height={32} src={iconInfo} />
            <p className="text-light">自分の記録</p>
          </div>
          <div
            className="flex gap-2 items-center cursor-pointer ml-2"
            onClick={() => setIsOpen(true)}
          >
            <img width={32} height={32} src={iconMenu} />
          </div>
          <Drawer
            open={isOpen}
            onClose={toggleDrawer}
            direction="right"
            className="bla bla bla"
          >
            <div>
              <div className="mt-12 flex justify-end">
                <p
                  className="bg-dark500 w-[24px] h-[24px] flex justify-center items-center text-primary400 font-[Inter] text-[19px] cursor-pointer"
                  onClick={() => setIsOpen(false)}
                >
                  X
                </p>
              </div>
              {menuDrawer?.map((item, index) => (
                <div
                  className="py-4 pl-8 bg-gray400 text-light cursor-pointer "
                  key={index}
                  onClick={() => {
                    setIsOpen(false);
                    navigate(item.path);
                  }}
                >
                  {item.label}
                </div>
              ))}
            </div>
          </Drawer>
        </nav>
      </div>
    </header>
  );
}

export default Header;
