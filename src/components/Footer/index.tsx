import backToTop from "../../assets/icons/backTop.png";

function Footer() {
  return (
    <footer className="bg-dark500 w-full h-[128px] relative">
      <ul className="container flex items-center h-full gap-x-[45px] gap-y-1 relative flex-wrap">
        <button
          className="fixed bottom-5 lg:bottom-48 w-[48px] h-[48px] border flex justify-center items-center border-gray400 rounded-full right-5 lg:right-28 hover:bg-primary300 transition-colors duration-300"
          onClick={() => {
            window.scroll({ top: 0, behavior: "auto" });
          }}
        >
          <img src={backToTop} width={15} height={8} alt="" />
        </button>
        <li>
          <p className="text-light">会員登録</p>
        </li>
        <li>
          <p className="text-light">運営会社</p>
        </li>
        <li>
          <p className="text-light">利用規約</p>
        </li>
        <li>
          <p className="text-light">個人情報の取扱について</p>
        </li>
        <li>
          <p className="text-light">お問い合わせ</p>
        </li>
      </ul>
    </footer>
  );
}

export default Footer;
