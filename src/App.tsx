import "./assets/fonts/Hiragino/Hiragino.otf";
import "./assets/fonts/Inter/Inter-VariableFont_slnt,wght.ttf";
import "./assets/fonts/Noto_Sans_JP/NotoSansJP-VariableFont_wght.ttf";
import { Routes, Route, Outlet } from "react-router-dom";
import MyPage from "./page/MyPage";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { routes } from "./routes/root";
import MyRecord from "./page/MyRecord";
import Columns from "./page/Columns";

function App() {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <>
            <Header />
            <Outlet />
            <Footer />
          </>
        }
      >
        <Route path={routes.HOME} element={<MyPage />}></Route>
        <Route path={routes.MY_RECORD} element={<MyRecord />}></Route>
        <Route path={routes.COLUMNS} element={<Columns />}></Route>
      </Route>
    </Routes>
  );
}

export default App;
