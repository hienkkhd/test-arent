import axiosClient from ".";
export interface Dashboard {
  calo: {
    time: string;
    value: number;
  };
  bodyChart?: {
    [key: string]: {
      value1: number;
      value2: number;
    };
  };
}
const dashboardApi = {
  getData(): Promise<Dashboard> {
    return axiosClient.get("/dashboard");
  },
};
export default dashboardApi;
