import axiosClient from ".";
export interface Category {
  title: string;
  image: string;
  sizeIcon: string;
}
const categoryApi = {
  getData(): Promise<Category[]> {
    return axiosClient.get("/categories");
  },
};
export default categoryApi;
