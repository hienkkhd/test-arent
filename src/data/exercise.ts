import axiosClient from ".";

export interface Exercise {
  title: string;
  time: string;
  calo: string;
}
const exerciseApi = {
  getData(): Promise<Exercise[]> {
    return axiosClient.get("/my-exercise");
  },
};
export default exerciseApi;
