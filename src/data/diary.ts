import axiosClient from ".";
export interface Diary {
  date: string;
  time: string;
  content: string;
}
export type DiaryParam = {
  _page: number;
  _limit: number;
};
const diaryApi = {
  getData(params: DiaryParam): Promise<Diary[]> {
    return axiosClient.get("/my-diary", {
      params,
    });
  },
};
export default diaryApi;
