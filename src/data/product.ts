import axiosClient from ".";

export interface ProductItem {
  image: string;
  date: string;
  category: string;
}
export type ProductParam = {
  _page: number;
  _limit: number;
  category: string | undefined;
};
const productApi = {
  getData({
    _page = 1,
    _limit = 8,
    category = undefined,
  }: ProductParam): Promise<ProductItem[]> {
    return axiosClient.get("/records", {
      params: {
        _page,
        _limit,
        category,
      },
    });
  },
};
export default productApi;
