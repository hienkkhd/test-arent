import axiosClient from ".";
export interface Column {
  content: string;
  date: string;
  time: string;
  image: string;
  tags: string[];
}
export type ColumnParam = {
  _page: number;
  _limit: number;
};
const columnApi = {
  getData(params: ColumnParam): Promise<Column[]> {
    return axiosClient.get("/my-column", { params });
  },
};
export default columnApi;
