import { useState, useEffect } from "react";
import columnApi, { Column, ColumnParam } from "../../data/column";


function ColumnList() {
  const [columnList, setColumnList] = useState<Column[]>([]);
  const [query, setQuery] = useState<ColumnParam>({
    _page: 1,
    _limit: 8,
  });
  useEffect(() => {
    (async () => {
      try {
        const data = await columnApi.getData(query);
        if (query._page === 1) {
          setColumnList(data);
        } else {
          setColumnList((prev) => [...prev, ...data]);
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }, [query]);

  return (
    <div className="mt-[56px]">
      <div className="container">
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-2 gap-y-5">
          {columnList &&
            columnList.map((item, index) => {
              return (
                <div className="flex flex-col relative" key={index}>
                  <div className="relative  w-[100%]">
                    <img
                      src={item.image}
                      className="h-[144px] w-[100%] object-cover"
                    />
                    <div className="absolute bottom-0 left-0 bg-primary300 pr-2 h-[24px] flex items-center">
                      <p className="text-light text-[15px] ml-2 font-[Inter] font-[400]">
                        {item.date}
                        <span className="ml-4">{item.time}</span>
                      </p>
                    </div>
                  </div>
                  <p className="text-[15px] line-clamp-2 mt-2 cursor-pointer">
                    {item.content}
                  </p>
                  <div className="flex gap-4 flex-wrap text-primary400 mt-1 text-[12px]">
                    {item?.tags &&
                      item?.tags.map((x, i) => (
                        <p className="cursor-pointer" key={i}>
                          #{x}
                        </p>
                      ))}
                  </div>
                </div>
              );
            })}
        </div>
      </div>
      <div className="mt-8 mb-16 flex justify-center">
        <button
          className="px-[76px] py-[15px] rounded-md"
          onClick={() => {
            setQuery((prev) => ({ ...prev, _page: prev._page + 1 }));
          }}
          style={{
            background:
              "linear-gradient(32.95deg, #FFCC21 8.75%, #FF963C 86.64%)",
          }}
        >
          <p className="text-[18px] text-light font-[300]">記録をもっと見る</p>
        </button>
      </div>
    </div>
  );
}

export default ColumnList;
