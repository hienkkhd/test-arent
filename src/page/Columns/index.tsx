import Categories from "./Categories";
import ColumnList from "./ColumnList";

function Columns() {
  return (
    <div>
      <Categories />
      <ColumnList />
      
    </div>
  );
}

export default Columns;
