function Categories() {
  return (
    <div className="mt-[56px]">
      <div className="container grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-7">
        <div className="py-[17px] bg-dark600 flex justify-center flex-col items-center cursor-pointer min-h-[144px]">
          <div className="text-center text-primary300 text-[22px]">
            <p className="line-clamp-2 font-[Inter]">RECOMMENDED COLUMN</p>
          </div>
          <div className="w-[56px] h-[1px] bg-light mt-2"></div>
          <p className="text-light text-[18px] mt-2">オススメ</p>
        </div>
        <div className="py-[17px] bg-dark600 flex justify-center flex-col items-center cursor-pointer min-h-[144px]">
          <div className="text-center text-primary300 text-[22px]">
            <p className="line-clamp-2 font-[Inter]">RECOMMENDED DIET</p>
          </div>
          <div className="w-[56px] h-[1px] bg-light mt-2"></div>
          <p className="text-light text-[18px] mt-2">ダイエット</p>
        </div>
        <div className="py-[17px] bg-dark600 flex justify-center flex-col items-center cursor-pointer min-h-[144px]">
          <div className="text-center text-primary300 text-[22px]">
            <p className="line-clamp-2 font-[Inter]">RECOMMENDED BEAUTY</p>
          </div>
          <div className="w-[56px] h-[1px] bg-light mt-2"></div>
          <p className="text-light text-[18px] mt-2">美容</p>
        </div>
        <div className="py-[17px] bg-dark600 flex justify-center flex-col items-center cursor-pointer min-h-[144px]">
          <div className="text-center text-primary300 text-[22px]">
            <p className="line-clamp-2 font-[Inter]">RECOMMENDED HEALTH</p>
          </div>
          <div className="w-[56px] h-[1px] bg-light mt-2"></div>
          <p className="text-light text-[18px] mt-2">健康</p>
        </div>
      </div>
    </div>
  );
}

export default Categories;
