import { useState, useEffect } from "react";
import exerciseApi, { Exercise } from "../../data/exercise";
function MyExercise() {
  const [listExercise, setListExercise] = useState<Exercise[]>([]);
  useEffect(() => {
    (async () => {
      try {
        const data: Exercise[] = await exerciseApi.getData();
        setListExercise(data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  return (
    <div id="my-exercise" className="mt-[56px]">
      <div className="container bg-dark500 pt-3 pb-6 pl-5 pr-8 overflow-hidden">
        <div className="flex">
          <h4 className="text-light font-[300] font-[Inter] text-[16px] uppercase leading-[20px] w-[96px]">
            MY EXERCISE
          </h4>
          <p className="text-light font-[300] text-[22px] font-[Inter]">
            2021.05.21
          </p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-5 gap-y-2 mt-2 h-[200px] overflow-y-auto custom-scroll">
          {listExercise &&
            listExercise?.map((item, index) => {
              return (
                <div className="flex border-b border-gray400 mr-7 " key={index}>
                  <div className="w-[7px] h-[7px] rounded-full bg-light mt-1"></div>
                  <div className="ml-3 flex-1">
                    <p className="text-light font-[200] text-[15px]">
                      {item.title}
                    </p>
                    <p className="text-primary300 text-[15px]">
                      {item.calo}kcal
                    </p>
                  </div>
                  <div className="text-primary300 text-[18px]">{item.time}</div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
}

export default MyExercise;
