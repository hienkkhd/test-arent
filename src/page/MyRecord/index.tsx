import BodyRecord from "./BodyRecord";
import Categories from "./Categories";
import MyDiary from "./MyDiary";
import MyExercise from "./MyExercise";


function MyRecord() {
  return (
    <>
      <Categories />
      <BodyRecord />
      <MyExercise />
      <MyDiary />
    </>
  );
}

export default MyRecord;
