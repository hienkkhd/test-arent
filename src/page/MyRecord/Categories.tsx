import recoment1 from "../../assets/Imagine/MyRecommend-1.png";
import recoment2 from "../../assets/Imagine/MyRecommend-2.png";
import recoment3 from "../../assets/Imagine/MyRecommend-3.png";

function Categories() {
  return (
    <div>
      <div className="mt-[56px]">
        <div className="container grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-x-20 gap-y-5">
          <div className="border-[22px] border-primary300 bg-[rgba(0,0,0,0.8)] relative">
            <img src={recoment1} className="max-h-[240px] md:max-h-[286px]" />
            <div className="flex flex-col justify-center items-center absolute top-0 left-0 w-full h-full">
              <p className="text-[25px] text-primary300 uppercase font-[Inter] font-[400]">
                BODY RECORD
              </p>
              <p className="text-[14px] px-[17px] bg-primary400 mt-1 text-light leading-[20px]">
                自分のカラダの記録
              </p>
            </div>
          </div>
          <div className="border-[22px] border-primary300 bg-[rgba(0,0,0,0.8)] relative">
            <img src={recoment2} className="max-h-[240px] md:max-h-[286px]" />
            <div className="flex flex-col justify-center items-center absolute top-0 left-0 w-full h-full">
              <p className="text-[25px] text-primary300 uppercase font-[Inter] font-[400]">
                MY EXERCISE
              </p>
              <p className="text-[14px] px-[17px] bg-primary400 mt-1 text-light leading-[20px]">
                自分のカラダの記録
              </p>
            </div>
          </div>
          <div className="border-[22px] border-primary300 bg-[rgba(0,0,0,0.8)] relative">
            <img src={recoment3} className="max-h-[240px] md:max-h-[286px]" />
            <div className="flex flex-col justify-center items-center absolute top-0 left-0 w-full h-full">
              <p className="text-[25px] text-primary300 uppercase font-[Inter] font-[400]">
                MY DIARY
              </p>
              <p className="text-[14px] px-[17px] bg-primary400 mt-1 text-light leading-[20px]">
                自分のカラダの記録
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Categories;
