import { useState, useEffect } from "react";
import diaryApi, { Diary, DiaryParam } from "../../data/diary";

function MyDiary() {
  const [listDiary, setListDiary] = useState<Diary[]>([]);
  const [query, setQuery] = useState<DiaryParam>({ _page: 1, _limit: 8 });
  useEffect(() => {
    (async () => {
      try {
        const data: Diary[] = await diaryApi.getData(query);
        if (query._page === 1) {
          setListDiary(data);
        } else setListDiary((prev) => [...prev, ...data]);
      } catch (error) {
        console.log(error);
      }
    })();
  }, [query]);

  return (
    <div className="mt-[56px]">
      <div className="container">
        <p>MY DIARY</p>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-3 mt-2">
          {listDiary &&
            listDiary?.map((item, index) => {
              return (
                <div className="p-4 border-[2px] border-dark400" key={index}>
                  <div className="text-[18px] leading-[22px] text-dark500">
                    <p>{item.date}</p>
                    <p>{item.time}</p>
                  </div>
                  <div className="mt-3">
                    <p className="text-[12px]">{item.content}</p>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
      <div className="mt-8 mb-16 flex justify-center">
        <button
          onClick={() => {
            setQuery((prev) => ({ ...prev, _page: prev._page + 1 }));
          }}
          className="px-[76px] py-[15px] rounded-md"
          style={{
            background:
              "linear-gradient(32.95deg, #FFCC21 8.75%, #FF963C 86.64%)",
          }}
        >
          <p className="text-[18px] text-light font-[300]">
            自分の日記をもっと見る
          </p>
        </button>
      </div>
    </div>
  );
}

export default MyDiary;
