import { useState } from "react";
import { Line } from "react-chartjs-2";
import {
  ArcElement,
  CategoryScale,
  Chart as ChartJS,
  LineElement,
  LinearScale,
  PointElement,
  Tooltip,
} from "chart.js";
ChartJS.register(
  ArcElement,
  Tooltip,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement
);
function BodyRecord() {
  const initLabels = ["Morning", "Lunch", "Dinner", "Snack"];
  const filterButton = ["日", "週", "月", "年"];
  const [filterActive, setFilterActive] = useState(filterButton[0]);
  const [labels, setLabels] = useState(initLabels);
  const dataLight = {
    labels,
    datasets: [
      {
        label: "Dataset 1",
        data: labels.map(() => Math.round(Math.random() * 100)),
        borderColor: "rgba(255, 255, 255,0.6)",
      },
      {
        label: "Dataset 2",
        data: labels.map(() => Math.round(Math.random() * 100)),
        borderColor: "#FFCC21",
      },
    ],
    borderWidth: 1,
  };
  const hanlcleChangeTime = (time: string) => {
    setFilterActive(time);
    switch (time) {
      case "日": {
        setLabels(initLabels);
        break;
      }
      case "週": {
        const arr = [
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday",
          "Sunday",
        ];
        setLabels(arr);
        break;
      }
      case "月": {
        const arr = Array(30)
          .fill(null)
          .map((_, index) => `${index + 1}`);
        setLabels(arr);
        break;
      }
      case "年": {
        const arr = [
          "January",
          "February",
          "March",
          "April",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ];
        setLabels(arr);
        break;
      }
      default: {
        return initLabels;
      }
    }
  };
  return (
    <div className="mt-[56px]">
      <div className="container bg-dark500 py-3 pl-5 pr-8">
        <div className="flex">
          <h4 className="text-light font-[300] font-[Inter] text-[16px] uppercase leading-[20px] w-[96px]">
            Body Record
          </h4>
          <p className="text-light font-[300] text-[22px] font-[Inter]">
            2021.05.21
          </p>
        </div>
        <div className="flex h-full mt-1 min-h-[208px]">
          <Line
            options={{
              responsive: true,
              maintainAspectRatio: false,
              scales: {
                x: {
                  grid: {
                    color: "#777777",
                    drawTicks: false,
                  },
                },
                y: {
                  ticks: {
                    display: false,
                  },
                  grid: {
                    display: false,
                  },
                },
              },
            }}
            data={dataLight}
          />
        </div>
        <div className="flex gap-5 mt-1">
          {filterButton?.map((item, index) => {
            return (
              <button
                key={index}
                className={`px-5 rounded-xl bg-light ${
                  filterActive === item ? "text-primary300" : "text-dark600"
                }`}
                onClick={() => hanlcleChangeTime(item)}
              >
                {item}
              </button>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default BodyRecord;
