import { useEffect, useState } from "react";
import productApi, { ProductItem, ProductParam } from "../../data/product";
import Categories from "./Categories";

function ProductList() {
  const [recordList, setRecordList] = useState<ProductItem[]>([]);
  const [query, setQuery] = useState<ProductParam>({
    _page: 1,
    _limit: 8,
    category: undefined,
  });

  useEffect(() => {
    (async () => {
      try {
        const data = await productApi.getData(query);
        setRecordList((prev) => {
          if (query._page === 1) {
            return data;
          }
          return [...prev, ...data];
        });
      } catch (error) {
        console.log(error);
      }
    })();
  }, [query]);
  const handleChangeCategory = (category: string) => {
    setQuery((prev) => {
      return { ...prev, _page: 1, category };
    });
  };
  return (
    <>
      <Categories onChangeCategory={handleChangeCategory} />
      <div className="container">
        <div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-2">
          {recordList &&
            recordList.map((item, index) => {
              return (
                <div
                  className="flex flex-col justify-center items-center relative"
                  key={index}
                >
                  <img src={item.image} />
                  <div className="absolute bottom-0 left-0 bg-primary300 w-[120px] h-[32px] flex items-center">
                    <p className="text-light text-[15px] ml-2 font-[Inter] font-[400]">
                      {item.date}
                      .{item.category}
                    </p>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
      <div className="mt-8 mb-16 flex justify-center">
        <button
          className="px-[76px] py-[15px] rounded-md"
          onClick={() => {
            setQuery((prev) => ({ ...prev, _page: prev._page + 1 }));
          }}
          style={{
            background:
              "linear-gradient(32.95deg, #FFCC21 8.75%, #FF963C 86.64%)",
          }}
        >
          <p className="text-[18px] text-light font-[300]">記録をもっと見る</p>
        </button>
      </div>
    </>
  );
}

export default ProductList;
