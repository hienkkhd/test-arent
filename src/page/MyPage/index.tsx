import Banner from "./Banner";
import ProductList from "./ProductList";

function MyPage() {
  return (
    <>
      <Banner />
      <ProductList />
    </>
  );
}

export default MyPage;
