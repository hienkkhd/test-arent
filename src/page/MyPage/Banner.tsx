import {
  ArcElement,
  CategoryScale,
  Chart as ChartJS,
  LineElement,
  LinearScale,
  PointElement,
  Tooltip,
} from "chart.js";
import { useEffect, useMemo, useState } from "react";
import { Line, Pie } from "react-chartjs-2";
import leftBanner from "../../assets/Imagine/main_photo.png";
import dashboardApi, { Dashboard } from "../../data/dashboard";

ChartJS.register(
  ArcElement,
  Tooltip,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement
);

function Banner() {
  const [dataDashboard, setDataDashboard] = useState<Dashboard>({
    calo: {
      time: "21.05",
      value: 75,
    },
  });
  useEffect(() => {
    (async () => {
      try {
        const data: Dashboard = await dashboardApi.getData();
        setDataDashboard(data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);
  const data = {
    labels: ["dsad"],
    datasets: [
      {
        label: "# of Votes",
        data: [dataDashboard?.calo.value, 100 - dataDashboard?.calo.value],
        backgroundColor: ["white", "transparent"],

        borderWidth: 1,
      },
    ],
  };
  const dataLight = useMemo(() => {
    const data = dataDashboard.bodyChart;
    if (data) {
      const labels = Object.keys(data).map((x) => `${x}月`);
      const datasets = [
        {
          label: "Dataset 1",
          data: Object.values(data).map((x) => x.value1),
          borderColor: "rgba(255, 255, 255,0.6)",
        },
        {
          label: "Dataset 2",
          data: Object.values(data).map((x) => x.value2),
          borderColor: "#FFCC21",
        },
      ];
      return { labels, datasets };
    }
  }, [dataDashboard.bodyChart]);

  return (
    <div className="flex flex-wrap">
      <div className="w-full lg:w-[42%] relative">
        <img src={leftBanner} alt="" className="w-full h-full object-cover" />
        {dataDashboard?.calo?.value && (
          <div className="flex justify-center items-center absolute top-0 left-0 w-full h-full">
            <div className="w-[180px] h-[180px] relative">
              <Pie
                data={data}
                options={{
                  borderColor: "transparent",
                  cutout: "96%",
                }}
              />
              <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center">
                <p className="text-light text-[16px]">
                  {dataDashboard.calo.time}{" "}
                  <span className="text-[24px]">
                    {dataDashboard.calo.value}%
                  </span>
                </p>
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="w-full lg:w-[58%] overflow-hidden bg-dark600 p-4 lg:pl-7 lg:py-4 lg:pr-16 xl:pr-20">
        {dataLight && (
          <Line
            options={{
              responsive: true,
              scales: {
                x: {
                  grid: {
                    color: "#777777",
                    drawTicks: false,
                  },
                  ticks: {
                    color: "white",
                  },
                },

                y: {
                  ticks: {
                    display: false,
                  },
                  grid: {
                    display: false,
                  },
                },
              },
            }}
            data={dataLight}
          />
        )}
      </div>
    </div>
  );
}

export default Banner;
