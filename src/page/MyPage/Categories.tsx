import { useEffect, useState } from "react";
import hexagon from "../../assets/Imagine/hexagon.png";
import categoryApi, { Category } from "../../data/category";
type Prop = {
  onChangeCategory: (title: string) => void;
};
function Categories({ onChangeCategory = () => {} }: Prop) {
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    (async () => {
      try {
        const data: Category[] = await categoryApi.getData();
        setCategories(data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  return (
    <div className="container py-7">
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-20 px-20">
        {categories &&
          categories.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => {
                  onChangeCategory(item.title);
                }}
              >
                <div className="flex flex-col justify-center items-center relative">
                  <img src={hexagon} alt="" />
                  <div className="absolute flex flex-col justify-center items-center">
                    <img src={item.image} width={item.sizeIcon} />
                    <p className="text-light mt-1 text-[20px]">{item.title}</p>
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default Categories;
